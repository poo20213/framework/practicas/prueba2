<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Datospersonales;
use app\models\Estudios;
use app\models\Experiencia;
use app\models\Otros;
use kartik\mpdf\Pdf;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(){
        
        //Consulta sin ejecutar
        //$consulta = Datospersonales::find();
        //$model=$consulta->one();
        
        // Consulata ejecutada y lo que me devuelve es un modelo con los datos
        $model = Datospersonales::find()->one();
        
        //Consulata ejecutada y lo que me devuelve es un array de modelos
        //$modelos = Datospersonales::find()->all();
        //$model = $modelos[0];
                
        return $this->render('index',[
            'model' => $model,
        ]);
    }
    
    public function actionAcademica() {
        
        // consulta de los estudios realizados
        $consulta = Estudios::find()->where([
            "complementarios" => 0
        ]);
        
        // Data provider cone el resultado de la consulat anterior
        // para mandarlo a un listview
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("academica",[
            "dataProvider" => $dataProvider,
            "titulo" => "Formacion academica"
        ]);
    }
    
    public function actionComplementaria() {
        // consulta de los estudios realizados
        $consulta = Estudios::find()->where([
            "complementarios" => 1
        ]);
        
        // Data provider cone el resultado de la consulat anterior
        // para mandarlo a un listview
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("academica",[
            "dataProvider" => $dataProvider,
            "titulo" => "Formacion complementaria"
        ]);
    }
    
    public function actionExp(){
        
        $consulta = Experiencia::find();
        
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render("exp",[
            "dataProvider" => $dataProvider,
            "titulo" => "Experiencia Laboral"
        ]);
    }
    
    public function actionOtros(){       
        
        $consulta = Otros::find()->select(["tipo"])->distinct();
        
        $dataProvider = new ActiveDataProvider([
            "query" => $consulta
        ]);
        
        return $this->render('otros',[
            "dataProvider" => $dataProvider, 
        ]);
        
    }
    
    public function actionPdf() {        
        
        $model = Datospersonales::find()->one();
        
        $formacion = Estudios::find()->where([
            "complementarios" => 0
        ]);
        
        $dataProviderFormacion = new ActiveDataProvider([
           "query" => $formacion
        ]);
        
        
        $complementaria = Estudios::find()->where([
            "complementarios" => 1
        ]);
        
        $dataProviderComplementaria = new ActiveDataProvider([
            "query" => $complementaria
        ]);
        
        
        $experiencia= Experiencia::find();
        
        $dataProviderExperiencia = new ActiveDataProvider([
            "query" => $experiencia
        ]);
       
        
        $otros = Otros::find()->select(["tipo"])->distinct();
        
        $dataProviderOtros = new ActiveDataProvider([
            "query" => $otros
        ]);
        
        
        $content = $this->renderPartial('pdf',[
            'model' => $model,
            "formacion" => $dataProviderFormacion,
            "complementaria" => $dataProviderComplementaria,
            "experiencia" => $dataProviderExperiencia,
            "otros" => $dataProviderOtros,
        ]);

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE, 
            // A4 paper format
            'format' => Pdf::FORMAT_A4, 
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT, 
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER, 
            // your html content input
            'content' => $content,  
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting 
            'cssFile' => ['@app/web/css/kv-mpdf-bootstrap.css','@app/web/css/site.css','@app/web/css/custom.css'],
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}', 
             // set mPDF properties on the fly
            'options' => [
               'title' => 'Curriculum',               
            ],
             // call mPDF methods on the fly
            'methods' => [ 
                'SetHeader'=>['Curriculum inventado Pedro J.'], 
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);
    
    // return the pdf output as per the destination setting
    return $pdf->render(); 
    }
    
}
