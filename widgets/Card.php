<?php

namespace app\widgets;
use yii\base\Widget;

class Card extends Widget{
    public $titulo;
    public $contenido;
    public $salida;

    public function init()
    {
        parent::init();
        
    }

    public function run()
    {
        $this->salida='<div class="card col-lg-12 p-0 mt-2">';
    
            $this->salida.= '<div class="card-header font-weight-bold">' . $this->titulo . '</div>';
            $this->salida.= '<div class="card-body">' . $this->contenido . '</div>';

        $this->salida.='</div>';
        return $this->salida;
    }
}
