﻿DROP DATABASE IF EXISTS prueba2;
CREATE DATABASE prueba2;
USE prueba2;

CREATE TABLE datosPersonales(
    id int AUTO_INCREMENT,
    nombre varchar(100),
    apellidos varchar(255),
    telefono varchar (20),
    email varchar(100),
    linkedin varchar(100),
    carnetConducir varchar(50),
    calle varchar(255),
    numero varchar(10),
    provincia varchar(100),
    poblacion varchar(100),
    cp varchar(5),
    foto varchar(100),
    PRIMARY KEY(id)
  );

CREATE TABLE estudios(
    id int AUTO_INCREMENT,
    titulo varchar(100),
    fechas varchar(255),
    centro varchar(255),
    complementarios boolean DEFAULT FALSE,
    PRIMARY KEY(id)
  );

CREATE TABLE competencias(
    id int AUTO_INCREMENT,
    descripcion varchar(500),
    idEstudio int,
    PRIMARY KEY (id) 
  );

CREATE TABLE experiencia(
    id int AUTO_INCREMENT,
    empresa varchar(100),
    poblacion varchar(100),
    puesto varchar(100),
    fechas varchar(100),
    PRIMARY KEY(id)
  );

CREATE TABLE funciones(
    id int AUTO_INCREMENT,
    descripcion varchar(1000),
    idExperiencia int,
    PRIMARY KEY(id)
  );

CREATE TABLE otros(
    id int AUTO_INCREMENT,
    nombre varchar(255),
    nivel enum('A1','A2','B1','B2','C1','C2',''),
    tipo varchar(255),
    PRIMARY KEY(id)
  );

ALTER TABLE funciones
  ADD CONSTRAINT fkFunciones_experiencia FOREIGN KEY (idExperiencia)
  REFERENCES experiencia(id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE competencias
  ADD CONSTRAINT fkCompetencias_estudio FOREIGN KEY (idEstudio)
    REFERENCES estudios (id) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO datosPersonales (nombre, apellidos, telefono, email, linkedin, carnetConducir, calle, numero, provincia, poblacion, cp, foto)
  VALUES ('Homer', 'Simpson', '486 555 621', 'homerj.@gmail.com', 'linkedin.com/homerj', 'A1, A2, B1 ', 'Calle Falsa', '123', 'La que sea', 'Springfield', '99547', 'foto.jpg');

INSERT INTO estudios (titulo, fechas, centro) VALUES 
('ingeniería nuclear', '1998-1993', 'Universidad de Springfield'),
('formación payaso','1998-2000','Instituto para payasos Krusty');

INSERT INTO competencias (descripcion, idEstudio) VALUES 
('Tabajar por dinero, dar el callo por dinero, no se que y no se cuanto pero tu dame dinero', 1),
('Dormir en el trabajo', 1),
('Provocar fusiones del nucleo en simuladores', 1),
('Librase de multas', 2),
('Tener deudas con la mafia', 2);

INSERT INTO estudios (titulo, fechas, centro, complementarios) VALUES 
('Programación orientada a objetos', '06/2021 - 12/2021', 'Alpe Formación', TRUE),
('Bases de datos relacionales', '01/2020 - 06/2020', 'Alpe Formación', TRUE);

INSERT INTO competencias (descripcion, idEstudio) VALUES 
('Programar', 3),
('Intentar comprender lo que se hace', 3),
('Morir al meter datos en Acces o SQL', 4),
('Progamador en lenguajes orientados a objetos', 4);

INSERT INTO experiencia (empresa, poblacion, puesto, fechas) VALUES 
('NASA', 'America', 'astronauta', '10/1999 - 02/2000'),
('Ayuntamiento de Springfield', 'Springfield', 'guardaespaldas', '02/2003-10/2006'),
('Autonomo', 'Springfield', 'conductor de ambulancia', '01/2007 - 06/2007'),
('Central nuclear de Springfield', 'Springfield', 'Inspector del sector 7G', '01/1998 - 12/2021');

INSERT INTO funciones (descripcion, idExperiencia)VALUES 
('Comer patatas en el espacio', 1),
('Pelearse con los demas astronautas', 1),
('Sabotear la estacion espacial rusa', 1),
('Proteger al alcalde', 2),
('Proteger a famosos', 2),
('Descubrir una sala de ordeño de ratas de la mafia', 2),
('Recivir vueltas de las empresas', 2),
('Dar vueltas con los pacientes por la ciudad', 3),
('Ser la rision del pueblo', 3),
('Amenazar a Flanders', 3),
('Provocar fusiones del nucleo', 4),
('Agredir al jefe', 4),
('Dormir en horas de trabajo', 4),
('Provocar el cierre de la central', 4),
('Provocar la locura y posterior suicidio de un compañero de trabajo', 4),
('Atropellar a inspectores nucleares', 4);

INSERT INTO otros (nombre, nivel, tipo) VALUES 
  ('Ingles', 'A1', 'Idioma'),
  ('Aleman(borracho)', 'C2', 'Idioma'),
  ('paginas web(mister X)', '', 'Informática'),
  ('hablar con el raton', '', 'Informática'),
  ('beber','', 'Aficiones'),
  ('dormir','', 'Aficiones'),
  ('Secuestro de submarinos','', 'Aficiones'),
  ('Engordar 200KG','', 'Ingeniería nuclear(nucelar)'),
  ('Subcontrata de pajaros','', 'Ingeniería nuclear(nucelar)'),
  ('TV','', 'aficiones');

