<?php
    use yii\helpers\Html;
    use yii\widgets\ListView;
?>
<div>
    <h1 style="float: left; width: 15cm">
    <?= $model->getNombreCompleto() ?>
    </h1>
    <div style="float: right; width: 3cm">
    <?= Html::img("@web/imgs/$model->foto",["class" => "foto"]); ?>
    </div>
</div>

<div>
    <h3>
        Dirección
    </h3>
    <div>
        <?= $model->getDireccion() ?>
        <br>
        <?= $model->getPoblacion() ?>
        <br>
        <?= $model->provincia ?>
    </div>
</div>

<div>
    <h3>
        Correo electronico
    </h3>
    <div>
        <?= $model->email ?>
        
    </div>
</div>

<div>
    <h3>
        Teléfono
    </h3>
    <div>
        <?= $model->telefono ?>
        
    </div>
</div>

<div>
    <h3>
        Linkedin
    </h3>
    <div>
        <?= $model->linkedin ?>
        
    </div>
</div>

<div>
    <h3>
        Carnets de conducir
    </h3>
    <div>
        <?= $model->carnetConducir ?>
        
    </div>
</div>


<div>
    <h3>
        Formación Academica
    </h3>
    <?php
        echo ListView::widget([
            "dataProvider" => $formacion,
            "itemView" => "pdf/_formacion",
            "layout" => "{items}",
            "options" => ["class" => "caja p-2"],
            "itemOptions" => ["class" => "elemento mb-3"]
        ]);            
    ?>
</div>

<div>
    <h3>
        Formación Complementaria
    </h3>
    <?php
        echo ListView::widget([
            "dataProvider" => $complementaria,
            "itemView" => "pdf/_formacion",
            "layout" => "{items}",
            "options" => ["class" => "caja p-2"],
            "itemOptions" => ["class" => "elemento mb-3"]
        ]);            
    ?>
</div>

<div>
    <h3>
        Experiencia profesional
    </h3>
    <?php
        echo ListView::widget([
            "dataProvider" => $experiencia,
            "itemView" => "pdf/_experiencia",
            "layout" => "{items}",
            "options" => ["class" => "caja p-2"],
            "itemOptions" => ["class" => "elemento mb-3"]
        ]);            
    ?>
</div>

<div>
    <h3>
        Otros Datos
    </h3>
    <?php
        echo ListView::widget([
            "dataProvider" => $otros,
            "itemView" => "pdf/_otrosDatos",
            "layout" => "{items}",
            "options" => ["class" => "caja p-2"],
            "itemOptions" => ["class" => "elemento col-print-5 p-2 mb-3 ml-1 ml-2 mr-3"]
        ]);            
    ?>
</div>

