<?php

use yii\widgets\ListView;

?>
<h2 class="bg-light p-2">
    <?= $titulo ?>
</h2>

<?= ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_academica",
    "layout" => "{items}",
    "options" => ["class" => "caja p-2"],
    "itemOptions" => ["class" => "elemento mb-3"]
]); ?>

