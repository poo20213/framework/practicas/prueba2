<?php 
use app\widgets\Card;
use yii\helpers\Html;
/*   
  echo yii\widgets\DetailView::widget([
       "model" => $model,
       "attributes" => [
           "nombre",
           "apellidos",
       ],
      "template" => function ($atributo){
          return "<div>" .$atributo['label'] . ":</div><div>" . $atributo['value'] . "<div>";
      }
   ]);
*/
   
 /*   foreach ($model as $campo){
?>
    <div><?= $campo ?></div>
    <div><?= $label ?></div>
<?php
       }*/

// a mano
?>
<div class="row">
<div class="col-lg-4">
    <?php echo Card::widget([
        "titulo" => "Foto Perfil",
        "contenido" => Html::img("@web/imgs/$model->foto",["class"=>"img-fluid"])
    ]);?>

    <?php echo Card::widget([
        "titulo" => "Nombre y apellidos",
        "contenido" => $model->getNombreCompleto()
    ]);?>
</div>
    
    
<div class="col-lg-8">
    <?php echo Card::widget([
        "titulo" => "Direccion",
        "contenido" => $model->getDireccion() . "<br>" . $model->getPoblacion() . "<br>" . $model->provincia
    ]);?>

    <?php echo Card::widget([
        "titulo" => "Correo Electronico",
        "contenido" => $model->email,
    ]);?>

    <?php echo Card::widget([
        "titulo" => "Telefono",
        "contenido" => $model->telefono,
    ]);?>

    <?php echo Card::widget([
        "titulo" => "Linkedin",
        "contenido" => $model->linkedin,
    ]);?>

    <?php echo Card::widget([
        "titulo" => "Carnets de conducir",
        "contenido" => $model->carnetConducir,
    ]);?>
</div>
</div>


