<?php

use yii\widgets\ListView;

?>
<h2 class="bg-light p-2">
    Otros 
</h2>

<?= ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => "_otros",
    "layout" => "{items}",
    "options" => ["class" => "caja p-2 row"],
    "itemOptions" => ["class" => "elemento otros col-lg-5 p-2 mb-3 ml-1 ml-2 mr-3"]
]); ?>